var eventBus = new Vue();

Vue.component('product', {
  props: {
    premium: {
      type: Boolean,
      required: true
    }
  },
  template: `
  <div class="product">
    <div class="product-image">
      <a v-bind:href="link">
        <img v-bind:src="image">
      </a>
    </div>

    <div class="product-info">
      <h1>{{ title }}</h1>
      <p v-if="inventory > 10">In Stock</p>
      <p v-else-if="inventory <= 10 && inventory > 0">Almost sold out!</p>
      <p v-else :class="{ oos: inventory == 0 }">Out of Stock</p>
      <p>Shipping: {{ shipping }}</p>

      <span v-show="onSale">On Sale!</span>

      <ul>
        <li v-for="detail in details">{{ detail }}</li>
      </ul>

      <div v-for="(variant, index) in variants" 
            :key="variant.id"
            class="color-box"
            :style="{ backgroundColor: variant.color }"
            @mouseover="updateProduct(index)">
      </div>

      <div>
        <button v-on:click="addToCart" 
          :disabled="(inventory == 0)"
          :class="{ disabledButton: inventory == 0 }"
          >Add to Cart
        </button>

        <button v-on:click="removeFromCart"
          >Remove from Cart
        </button>
      </div>
    </div>

    <product-tabs :reviews="reviews"></product-tabs>
    
  </div>
  `,
  data() {
    return {
      product: 'Socks',
      brand: 'Vue Mastery',
      link: 'https://pronet.com.uy/',
      selectedVariant: 0,
      onSale: false,
      details: ["80% cotton", "20% polyester", "Gender neutral"],
      variants: [
      {
        id: 225,
        color: "green",
        image: './assets/vmSocks-green.jpg',
        quantity: 12
      },
      {
        id: 226,
        color: "blue",
        image: './assets/vmSocks-blue.jpg',
        quantity: 8
      }],
      cart: 0,
      reviews: []
    }
  },
  methods: {
    addToCart: function() {
      this.$emit('add-to-cart', this.variants[this.selectedVariant].id);
    },
    removeFromCart () {
      this.$emit('remove-from-cart', this.variants[this.selectedVariant].id);
    },
    updateProduct(index) {
      this.selectedVariant = index;
    }
  },
  computed: {
    title() {
      return (this.brand + ' ' + this.product);
    },
    image() {
      return (this.variants[this.selectedVariant].image);
    },
    inventory() {
      return (this.variants[this.selectedVariant].quantity);
    },
    shipping() {
      if (this.premium) {
        return 'Free';
      }
      return '$2,99';
    }
  },
  mounted() {
    eventBus.$on('review-submitted', productReview => {
      this.reviews.push(productReview)
    })
  }
})

Vue.component('product-review', {
  template: 
  `
  <form class="review-form" @submit.prevent="onSubmit">
  <p v-if="errors.length">
    <b>Please correct the following error(s):</b>
    <ul>
      <li v-for="error in errors">{{ error }}</li>
    </ul>
  </p>
    <p>
      <label for="name">Name:</label>
      <input id="name" v-model="name" placeholder="name">
    </p>
    <p>
      <label for="review">Review:</label>
      <textarea id="review" v-model="review"></textarea>
    </p>
    <p>
      <label for="rating">Rating:</label>
      <select id="rating" v-model.number="rating">
        <option>5</option>
        <option>4</option>
        <option>3</option>
        <option>2</option>
        <option>1</option>
      </select>
    </p>
    <p>
      <label>Would you recommend this product?</label>
      <input type="radio" id="recommended"
    </p>
    <p>
      <input type="submit" value="Submit">
    </p>
  </form>
  `,
  data() {
    return {
      name: null,
      review: null,
      rating: null,
      errors: []
    }
  },
  methods: {
    onSubmit() {
      if (this.name && this.review && this.rating){
        let productReview = {
          name: this.name,
          review: this.review,
          rating: this.rating

        }
        eventBus.$emit('review-submitted', productReview);
        this.name = null;
        this.review = null;
        this.rating = null;
        console.log(productReview);
      } else {
        // This empties the errors array
        this.errors.splice(0,this.errors.length);
        if (!this.name) this.errors.push("Name required");
        if (!this.review) this.errors.push("Review required");
        if (!this.rating) this.errors.push("Rating required");
      }
    }
  }
})

Vue.component('product-tabs', {
  props: {
    reviews: {
      type: Array,
      required: true
    }
  },
  template: `
    <div>
      <div>
        <span class="tab" 
          :class="{ activeTab: selectedTab === tab }"
          v-for="(tab, index) in tabs" 
          :key="index"
          @click="selectedTab = tab">
        {{ tab }}</span>
      </div>
    


      <div v-show="selectedTab === 'Reviews'">
        <h2>Reviews</h2>
        <p v-if="!reviews.length">There're no reviews yet.</p>
        <ul>
          <li v-for="review in reviews">
            <p>{{ review.name }}</p>
            <p>Rating:{{ review.rating }}</p>
            <p>{{ review.review }}</p>
          </li>
        </ul>
      </div>

      <product-review v-show="selectedTab === 'Make a Review'"></product-review>

    </div>
  `,
  data() {
    return {
      tabs: ['Reviews', 'Make a Review'],
      selectedTab: 'Reviews'
    }
  }
})

var app = new Vue({
  el: '#app',
  data: {
    premium: true,
    cart: []
  },
  methods: {
    updateCart(id) {
      this.cart.push(id);
    },
    removeFromCart(id) {
      console.log("this.cart.lenght" + this.cart.lenght);
      console.log(this.cart);

      for (let i = 0; i < this.cart.length; i++) {
        console.log(i + ' === ' + id);
        if (this.cart[i] === id) {
          this.cart.splice(i, 1);
        }
      }
    }
  }
})